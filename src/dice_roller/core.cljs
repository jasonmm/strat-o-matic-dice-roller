(ns dice-roller.core
  (:require
    [reagent.dom :as rdom]
    [re-frame.core :as re-frame]
    [dice-roller.routes :as routes]
    [dice-roller.events :as events]
    [dice-roller.views :as views]))

(def debug?
  ^boolean goog.DEBUG)

(defn dev-setup
  []
  (when debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root
  []
  (re-frame/clear-subscription-cache!)
  (rdom/render [views/main]
               (.getElementById js/document "app")))

(defn ^:export init
  []
  (routes/start!)
  (re-frame/dispatch-sync [::events/initialize-app])
  (dev-setup)
  (mount-root))

(defn ^:dev/after-load start
  "Necessary for hot-reloading.
  See https://stackoverflow.com/a/65908793 and linked blog post."
  []
  (mount-root))

