(ns dice-roller.subs
  (:require
    [re-frame.core :refer [reg-sub]]))

(def <sub (comp deref re-frame.core/subscribe))

(reg-sub
  ::active-panel
  (fn [db _]
    (:active-panel db)))

(reg-sub
  ::card-dice
  (fn [db _]
    (:dice db)))

(reg-sub
  ::card-dice-for-display
  :<- [::card-dice]
  (fn [{:keys [column row-1 row-2]}]
    (cond-> {:column "X"
             :row-1  "X"
             :row-2  "X"}
            column (assoc :column column)
            row-1 (assoc :row-1 row-1)
            row-2 (assoc :row-2 row-2))))

(reg-sub
  ::twenty-sided-dice
  (fn [db _]
    (get-in db [:dice :twenty])))

(reg-sub
  ::history
  (fn [db _]
    (:history db)))

(reg-sub
  ::history?
  :<- [::history]
  (fn [history]
    (seq history)))

