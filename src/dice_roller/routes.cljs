(ns dice-roller.routes
  (:require
    [re-frame.core :as re]
    [goog.events :as gevents]
    [goog.history.EventType :as EventType]
    [bidi.bidi :as bidi]
    [dice-roller.events :as events])
  (:import goog.History))

(def routes
  ["/" {"" :home}])

(defn on-navigate
      [^js/goog.events.EventType e]
      (re/dispatch [::events/route-dispatch (or (bidi/match-route routes (.-token e))
                                                {:handler :home})]))

(defn start!
      []
      (doto (History.)
            (gevents/listen EventType/NAVIGATE on-navigate)
            (.setEnabled true)))

