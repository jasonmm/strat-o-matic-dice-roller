(ns dice-roller.events
  (:require
    [re-frame.core :refer [reg-event-fx reg-event-db dispatch]]
    [day8.re-frame.async-flow-fx :as async-flow-fx]
    [dice-roller.db]))

(defn roll-dice
  ([] (roll-dice 6))
  ([sides] (-> sides dec rand-int inc)))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(reg-event-fx
  ::route-dispatch
  (fn [{:keys [db]} [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home {:dispatch [::set-active-panel :home]}))))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    (let [default-db dice-roller.db/default-db]
      {:db       default-db
       :dispatch [::set-active-panel :home]})))

(reg-event-db
  ::twenty-sided-rolled
  (fn [db [_ dice-value]]
    (let [history              (:history db)
          ; lists are conj'd to the beginning, so the first item is the most recent.
          last-history-item    (first history)
          last-with-20         (update last-history-item :twenty conj dice-value)
          history-without-last (drop 1 history)]
      (assoc db :history (conj history-without-last last-with-20)))))

(reg-event-db
  ::update-dice-history
  (fn [db _]
    (update db :history conj (merge {:key (js/Date.)} (:dice db)))))

(reg-event-db
  ::roll-card-die
  (fn [db [_ die-keyword]]
    (assoc-in db [:dice die-keyword] (roll-dice))))

(reg-event-db
  ;; Handle a JS `setTimeout` expiration. Clears the timer identified by the
  ;; value in `:timeout-id` in app-db.
  ::wait-over
  (fn [db _]
    (js/clearTimeout (:timeout-id db))
    (dissoc db :timeout-id)))

(reg-event-db
  ;; Set a JS timeout for `ms` milliseconds. The `::wait-over` event is
  ;; dispatched when the timer expires.
  ::wait
  (fn [db [_ ms]]
    (let [timeout-id (js/setTimeout #(dispatch [::wait-over]) ms)]
      (assoc db :timeout-id timeout-id))))

(defn roll-three-dice-flow []
  {:id      ::roll-three-dice-flow
   :db-path [:async-flow :roll-three-dice-flow]
   :rules   [{:when       :seen?
              :events     ::wait-over
              :dispatch-n [[::roll-card-die :row-1]
                           [::roll-card-die :row-2]
                           [::update-dice-history]]
              :halt?      true}]})

(reg-event-fx
  ::roll-three-dice
  (fn [{:keys [db]} _]
    {:async-flow (roll-three-dice-flow)
     :db         (dissoc db :dice)
     :fx         [[:dispatch [::roll-card-die :column]]
                  [:dispatch [::wait 250]]]}))

(reg-event-fx
  ::roll-twenty-sided-dice
  (fn [{:keys [db]} _]
    (let [dice-value (roll-dice 20)]
      {:db       (assoc-in db [:dice :twenty] dice-value)
       :dispatch [::twenty-sided-rolled dice-value]})))

(reg-event-db
  ::clear-history
  (fn [db _]
    (dissoc db :history :dice)))
