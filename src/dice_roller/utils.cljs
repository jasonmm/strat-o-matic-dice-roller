(ns dice-roller.utils
  (:require
    [goog.string :as gstring]
    [goog.string.format]))

(defn format-history-date
  "Format the date `d` for the history section in the UI."
  [d]
  (str
    ;(.getFullYear d) "-"
    ;(gstring/format "%02d" (inc (.getMonth d))) "-"
    ;(gstring/format "%02d" (.getDate d)) " "
    (gstring/format "%02d" (.getHours d)) ":"
    (gstring/format "%02d" (.getMinutes d)) ":"
    (gstring/format "%02d" (.getSeconds d)) "."
    (gstring/format "%03d" (.getMilliseconds d))))

(defn history-item-key
  [item]
  (let [d (:key item)]
    (+ (* (.getTime d) 1000)
       (.getMilliseconds d))))