(ns dice-roller.views
  (:require
    [re-frame.core :refer [dispatch]]
    [clojure.string :as str]
    [dice-roller.subs :as subs :refer [<sub]]
    [dice-roller.events :as events]
    [dice-roller.utils :as u]))


(defn card-dice []
  (let [dice (<sub [::subs/card-dice-for-display])]
    [:div.card-dice
     [:div.die.column-die (:column dice)]
     [:div.die.row-die (:row-1 dice)]
     [:div.die.row-die (:row-2 dice)]]))

(defn twenty-sided-die []
  (let [dice-value (or (<sub [::subs/twenty-sided-dice]) "X")]
    [:div.card-dice
     [:div.die.twenty-die dice-value]]))

(defn twenty-sided-dice-button []
  [:div
   [:button.roll-twenty-sided-dice
    {:type     :button
     :on-click #(dispatch [::events/roll-twenty-sided-dice])}
    "Roll Twenty Sided"]])

(defn twenty-sided-dice-area []
  [:<>
   [:hr]
   [twenty-sided-die]
   [twenty-sided-dice-button]])

(defn card-dice-button []
  [:div
   [:button.roll-3-dice
    {:type     :button
     :on-click #(dispatch [::events/roll-three-dice])}
    "Roll!"]])

(defn history-dice
  "Given a history item return a string representing the card dice roll."
  [item]
  [:<>
   [:span.column-die (:column item)] "  "
   [:span.row-die (:row-1 item)] "-"
   [:span.row-die (:row-2 item)]])

(defn history-item
  [item]
  [:div.history-item {:data-key (u/history-item-key item)}
   [:div.timestamp (u/format-history-date (:key item))]
   [:div.history-card-dice (history-dice item)]
   [:div.twenty-sided-dice
    (when (seq (:twenty item))
      [:span.twenty-die (str/join ", " (reverse (:twenty item)))])]])

(defn history []
  (let [history-items (<sub [::subs/history])]
    [:div.history
     [:div.title
      [:strong "History"]
      [:button.clear-history
       {:on-click #(dispatch [::events/clear-history])}
       "Clear History"]]
     [:div.history-items
      (for [item history-items]
        ^{:key (u/history-item-key item)}
        [history-item item])]]))


(defn home-panel []
  [:div
   [:div.title [:strong "Strat-o-Matic Dice Roller"]]
   [card-dice]
   [card-dice-button]
   (when (<sub [::subs/history?])
     [twenty-sided-dice-area])
   [:hr]
   [history]])

(defn main []
  (case (<sub [::subs/active-panel])
    :home [home-panel]
    [:div]))
