const CACHE_STATIC_NAME = "dice-roller-3";
const STATIC_FILES = [
  '.',
  'index.html',
  'favicon.ico',
  'apple-touch-icon.png',
  'android-chrome-512x512.png',
  'android-chrome-192x192.png',
  'js/compiled/app.js',
  'css/styles.css',
];

self.addEventListener('install', function (event) {
  console.log('[Service Worker] Installing Service Worker ...', event);
  event.waitUntil(
    caches
      .open(CACHE_STATIC_NAME)
      .then(function (cache) {
        console.log('[Service Worker] Precaching ...', cache);

        cache
          .addAll(STATIC_FILES)
          .then(() => console.log('[Service Worker] Static files added to cache'))
          .catch((err) => console.log('[Service Worker] Error adding cache files', err));
      })
  )
});

self.addEventListener('activate', function (event) {
  console.log('[Service Worker] Activating Service Worker ....', event);
  event.waitUntil(
    caches.keys()
      .then(keyList => {
        return Promise.all(keyList.map(function (key) {
          if (key !== CACHE_STATIC_NAME) {
            console.log('[Service Worker] Removing old cache', key);
            return caches.delete(key);
          }
        }));
      })
  );
  return self.clients.claim();
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches
      .match(event.request)
      .then(response => {
        if (response) {
          return response;
        } else {
          return fetch(event.request);
        }
      })
  );
});
